<?php
include_once('config.php');
function createTransaction($shopID, $amount, $description, $operationNumber){
    global $ducopayApiKey;
    $url = 'https://pay.duinocoin.com/api/v1/createTransaction';
    $array = array(
      'shopID'=>$shopID,
      'apikey'=>$ducopayApiKey,
      'amount'=>$amount,
      'description'=>$description,
      'operationNumber'=>$operationNumber
    );
    // Create a new cURL resource
    $ch = curl_init($url);
    $payload = json_encode($array);
  
    // Attach encoded JSON string to the POST fields
    curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
  
    // Set the content type to application/json
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
  
    // Return response instead of outputting
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  
    // Execute the POST request
    $result = json_decode(curl_exec($ch));
    // Close cURL resource
    curl_close($ch);
    return($result);
}


if(isset($_POST['email'])){
    // https://medium.com/@hCaptcha/using-hcaptcha-with-php-fc31884aa9ea
    $data = array(
        'secret' => $hcaptchaSecret,
        'response' => $_POST['h-captcha-response']
    );
    $verify = curl_init();
    curl_setopt($verify, CURLOPT_URL, "https://hcaptcha.com/siteverify");
    curl_setopt($verify, CURLOPT_POST, true);
    curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($verify);

    $responseData = json_decode($response);

    if($responseData->success) {
        // if captcha was successful
        $email = $_POST['email'];
        $status = 'created';
        $stmt = $conn->prepare("INSERT INTO orders (email, status) VALUES (?, ?)");
        $stmt->bind_param("ss", $email, $status);
        $stmt->execute();
        $lastID = $stmt->insert_id;
        $stmt->close();
        $conn->close();

        $amount = 1;

        $createTransaction = createTransaction($ducopayShopID, $amount, $ducopayDescription, $lastID);
        $uid = $createTransaction->uid;
        $url = "https://pay.duinocoin.com/pay/$ducopayShopID/$uid";

        header('Location: ' . $url);
        
        
    } 
    else {
        // if captcha was invalid
        echo "Invalid captcha!";
    }
    
}

?>
