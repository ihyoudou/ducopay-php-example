<?php 
include('config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src='https://www.hCaptcha.com/1/api.js' async defer></script>
    <title>DucoPay Example</title>
</head>
<body>
    <h3>DucoPay example</h3>
    <p>Buy lorem ipsum stright to your mailbox!</p>
    <form action="process.php" method="POST">
        <label>Enter your email</label>
        <input type="email" name="email">
        <div class="h-captcha" data-sitekey="<?php echo $hcaptchaPublic; ?>"></div>
        <button type="submit">Submit</button>
    </form>
    <p>cost: 1 DUCO</p>
</body>
</html>