<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DucoPay Example</title>
</head>
<body>
    <h3>Thank you for your purchase!</h3>
    <p>If payment was sucessful, lorem ipsum will arive in your mailbox shortly!</p>
    <p>Please note that not all mailservers like 'lorem ipsum' content, so it might be in Spam inbox!<br>
        P.S: no refunds!
    </p>
</body>
</html>