<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit70bbc3b43b72d28b60c3257a62ed1acd
{
    public static $prefixLengthsPsr4 = array (
        'j' => 
        array (
            'joshtronic\\' => 11,
        ),
        'P' => 
        array (
            'PHPMailer\\PHPMailer\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'joshtronic\\' => 
        array (
            0 => __DIR__ . '/..' . '/joshtronic/php-loremipsum/src',
        ),
        'PHPMailer\\PHPMailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpmailer/phpmailer/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit70bbc3b43b72d28b60c3257a62ed1acd::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit70bbc3b43b72d28b60c3257a62ed1acd::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit70bbc3b43b72d28b60c3257a62ed1acd::$classMap;

        }, null, ClassLoader::class);
    }
}
