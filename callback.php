<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';
include_once('config.php');

$receivedBody = json_decode(file_get_contents('php://input'));
var_dump($receivedBody);
$combined = $receivedBody->opNumber.$receivedBody->transactionID;
$signature = base64_encode(hash_hmac('sha256', $combined, $ducopayApiKey));

if($signature == $receivedBody->signature){
    //Create an instance; passing `true` enables exceptions
    $mail = new PHPMailer(true);
    $lipsum = new joshtronic\LoremIpsum();
    $exampleText = $lipsum->paragraphs(2, 'p');
    // if signature is valid
    
    // get email
    $stmt = $conn->prepare("SELECT email FROM orders WHERE ID=?");
    $stmt->bind_param("i", $receivedBody->opNumber);
    $stmt->execute();
    $userEmail = $stmt->get_result()->fetch_assoc()['email'];

    try {
        //Server settings
        // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host       = $smtpHost;                     //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = $mailUsername;                     //SMTP username
        $mail->Password   = $mailPassword;                               //SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;            //Enable START TLS encryption
        $mail->SMTPAutoTLS = false; 
        $mail->Port       = $smtpPort;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`
    
        //Recipients
        $mail->setFrom($mailUsername, 'Lorem Ipsum Store');
        $mail->addAddress($userEmail);     //Add a recipient
    
        //Content
        $mail->Subject = 'Thanks for purchase from my store!';
        $mail->Body    = "Thanks for purchase from the <b>Lorem Ipsum store</b>.<BR>There is your order:<BR>$exampleText";
        $mail->AltBody = "Thanks for purchase from the Lorem Ipsum store.%0D%0AThere is your order:%0D%0A$exampleText";
    
        $mail->send();
        echo 'Message has been sent';
        // update order to complete
        $stmt = $conn->prepare("UPDATE orders SET status='completed' WHERE ID=?");
        $stmt->bind_param("i", $receivedBody->opNumber);
        $stmt->execute();
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
    echo "ok";
    $stmt->close();
    $conn->close();
} else {
    // if signature check fails
    echo "ko";
}
?>